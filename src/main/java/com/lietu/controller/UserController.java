package com.lietu.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lietu.entity.User;
import com.lietu.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @ResponseBody
    @RequestMapping(value = "/queryAllUser", produces = {"application/json;charset=UTF-8"})
    //http://localhost:8080/user/queryAllUser
    public List<Map<String,Object>> addUser(){
	    //pageNum 开始页数
	    //pageSize 每页显示的数据条数
        return userService.selectAllUser(1,3);//查询第一页,3条数据
    }
	//


}
