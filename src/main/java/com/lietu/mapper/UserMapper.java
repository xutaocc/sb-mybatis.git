package com.lietu.mapper;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.lietu.entity.User;

/**
*  user
* @author 劣徒5520 2020-01-17
*/
@Repository
public interface UserMapper  {
	List<Map<String,Object>> selectAllUser();

}
