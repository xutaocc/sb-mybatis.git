package com.lietu.mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 *  tt_loan
 * @author 大狼狗 2020-04-11
 */
@Repository
public interface TtLoanMapper {
    List<Map<String,Object>> selectLoanInfo();



}
