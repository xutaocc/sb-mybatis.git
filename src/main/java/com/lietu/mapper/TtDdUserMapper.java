package com.lietu.mapper;

import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
 * tt_dd_user
 * @author 大狼狗
 * @date 2020/04/11
 */

@Repository
public interface TtDdUserMapper {
    List<Map<String,Object>>  selectTtDdUserAboutInfo();



}
