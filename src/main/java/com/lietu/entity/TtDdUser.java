package com.lietu.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *  tt_dd_user
 * @author 大狼狗 2020-04-11
 */
public class TtDdUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ddid
     */
    private Long ddId;

    /**
     * 钉钉用户id
     */
    private String ddUserId;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 标识
     */
    private Integer flag;

    /**
     * status
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * create_date
     */
    private Date createDate;

    /**
     * create_by
     */
    private Long createBy;

    /**
     * update_date
     */
    private Date updateDate;

    /**
     * update_by
     */
    private Long updateBy;

    /**
     * 版本
     */
    private Integer ver;


    public TtDdUser() {
    }

    public Long getDdId() {
        return ddId;
    }

    public void setDdId(Long ddId) {
        this.ddId = ddId;
    }

    public String getDdUserId() {
        return ddUserId;
    }

    public void setDdUserId(String ddUserId) {
        this.ddUserId = ddUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Integer getVer() {
        return ver;
    }

    public void setVer(Integer ver) {
        this.ver = ver;
    }

}
