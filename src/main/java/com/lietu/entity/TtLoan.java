package com.lietu.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *  tt_loan
 * @author 大狼狗 2020-04-11
 */
public class TtLoan implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 申请单id
     */
    private Long loanId;

    /**
     * 借款单号
     */
    private String loanNo;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 身份证号码
     */
    private String idCard;

    /**
     * 手机号码
     */
    private String mobilePhone;

    /**
     * 征信结果信息
     */
    private String creditResult;

    /**
     * 客户经理
     */
    private String salesManager;

    /**
     * 分组
     */
    private String groupName;

    /**
     * gps个数
     */
    private String gpsQuantity;

    /**
     * 额度：大于20万、小于20万
     */
    private Integer loanLimit;

    /**
     * 是否复议
     */
    private Integer isReconsider;

    /**
     * 进件日期
     */
    private Date applyDate;

    /**
     * 单据状态
     */
    private Integer status;

    /**
     * create_date
     */
    private Date createDate;

    /**
     * create_by
     */
    private Long createBy;

    /**
     * update_date
     */
    private Date updateDate;

    /**
     * update_by
     */
    private Long updateBy;

    /**
     * 版本
     */
    private Integer ver;

    /**
     * 分组代码
     */
    private String groupCode;

    /**
     * 内勤分配处理人员
     */
    private Long distributeUserId;

    /**
     * 分配状态
     */
    private Integer distributeStatus;

    /**
     * 征信状态
     */
    private Integer creditStatus;

    /**
     * 报送银行
     */
    private Integer sendBank;

    /**
     * 是否特批费率
     */
    private Integer isSpecialRate;

    /**
     * 是否免家访费
     */
    private Integer isFreeHome;

    /**
     * 直客处理状态：未处理、已处理
     */
    private Integer directStatus;


    public TtLoan() {
    }

    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

    public String getLoanNo() {
        return loanNo;
    }

    public void setLoanNo(String loanNo) {
        this.loanNo = loanNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getCreditResult() {
        return creditResult;
    }

    public void setCreditResult(String creditResult) {
        this.creditResult = creditResult;
    }

    public String getSalesManager() {
        return salesManager;
    }

    public void setSalesManager(String salesManager) {
        this.salesManager = salesManager;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGpsQuantity() {
        return gpsQuantity;
    }

    public void setGpsQuantity(String gpsQuantity) {
        this.gpsQuantity = gpsQuantity;
    }

    public Integer getLoanLimit() {
        return loanLimit;
    }

    public void setLoanLimit(Integer loanLimit) {
        this.loanLimit = loanLimit;
    }

    public Integer getIsReconsider() {
        return isReconsider;
    }

    public void setIsReconsider(Integer isReconsider) {
        this.isReconsider = isReconsider;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Integer getVer() {
        return ver;
    }

    public void setVer(Integer ver) {
        this.ver = ver;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public Long getDistributeUserId() {
        return distributeUserId;
    }

    public void setDistributeUserId(Long distributeUserId) {
        this.distributeUserId = distributeUserId;
    }

    public Integer getDistributeStatus() {
        return distributeStatus;
    }

    public void setDistributeStatus(Integer distributeStatus) {
        this.distributeStatus = distributeStatus;
    }

    public Integer getCreditStatus() {
        return creditStatus;
    }

    public void setCreditStatus(Integer creditStatus) {
        this.creditStatus = creditStatus;
    }

    public Integer getSendBank() {
        return sendBank;
    }

    public void setSendBank(Integer sendBank) {
        this.sendBank = sendBank;
    }

    public Integer getIsSpecialRate() {
        return isSpecialRate;
    }

    public void setIsSpecialRate(Integer isSpecialRate) {
        this.isSpecialRate = isSpecialRate;
    }

    public Integer getIsFreeHome() {
        return isFreeHome;
    }

    public void setIsFreeHome(Integer isFreeHome) {
        this.isFreeHome = isFreeHome;
    }

    public Integer getDirectStatus() {
        return directStatus;
    }

    public void setDirectStatus(Integer directStatus) {
        this.directStatus = directStatus;
    }

}
