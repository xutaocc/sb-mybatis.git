package com.lietu.entity;

import java.io.Serializable;

/**
*  user
* @author lietu5520 2020-01-17
*/


/**
 * 
CREATE TABLE `user` (
  `uid` tinyint(2) NOT NULL AUTO_INCREMENT,
  `uname` varchar(20) DEFAULT NULL,
  `usex` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8

sql-java代码生成器
http://java.bejson.com/generator/
 * 
 * @author Administrator
 *
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * uid
    */
    private Integer uid;

    /**
    * uname
    */
    private String uname;

    /**
    * usex
    */
    private String usex;


    public User() {
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUsex() {
        return usex;
    }

    public void setUsex(String usex) {
        this.usex = usex;
    }

	@Override
	public String toString() {
		return "User [uid=" + uid + ", uname=" + uname + ", usex=" + usex + "]";
	}
    

}
