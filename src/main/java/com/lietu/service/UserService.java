package com.lietu.service;

import java.util.List;
import java.util.Map;

import com.lietu.entity.User;

public interface UserService {
	public List<Map<String,Object>> selectAllUser(int pageNum, int pageSize);

}
