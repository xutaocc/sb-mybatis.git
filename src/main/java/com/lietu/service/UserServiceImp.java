package com.lietu.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.lietu.entity.User;
import com.lietu.mapper.UserMapper;

@Service
public class UserServiceImp implements UserService{
	@Autowired
	public UserMapper userMapper;
	   /*
	    * 这个方法中用到了我们开头配置依赖的分页插件pagehelper
	    * 很简单，只需要在service层传入参数，然后将参数传递给一个插件的一个静态方法即可；
	    * pageNum 开始页数
	    * pageSize 每页显示的数据条数
	    * */
	@Override
	public List<Map<String,Object>> selectAllUser(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
		// TODO 自动生成的方法存根
		return userMapper.selectAllUser();
	}
	

}
