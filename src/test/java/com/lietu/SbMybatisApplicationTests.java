package com.lietu;

import com.lietu.mapper.TtDdUserMapper;
import com.lietu.mapper.TtLoanMapper;
import com.lietu.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
class SbMybatisApplicationTests {
	@Autowired
	public UserMapper userMapper;
	@Autowired
	public TtDdUserMapper ttDdUserMapper;
	@Autowired
	public com.lietu.mapper.TtLoanMapper ttLoanMapper;
	@Test
	void contextLoads() {
		List<Map<String, Object>> maps = userMapper.selectAllUser();
		System.out.println(maps);
	}
	@Test
	void contextLoads2() {
		List<Map<String, Object>> maps = ttDdUserMapper.selectTtDdUserAboutInfo();
		System.out.println(maps);
	}

	@Test
	void contextLoads3() {
		List<Map<String, Object>> maps = ttLoanMapper.selectLoanInfo();
		System.out.println(maps);
	}

}
